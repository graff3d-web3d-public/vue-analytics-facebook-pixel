// Inspired by https://github.com/MatteoGabriele/vue-analytics
import { injectFacebookPixelToWindow } from './pixel'
import FacebookPixelError from './FacebookPixelError'

class FacebookPixel {
  constructor () {
    this.init = this.init.bind(this)
    this.query = this.query.bind(this)
    this.event = this.event.bind(this)
    this.install = this.install.bind(this)
    this._config = {
      router: undefined,
      autoPageView: false,
      debug: false,
      excludedRoutes: []
    }
  }
  _fbqEnabled () {
    return !!window.fbq && typeof window.fbq === 'function'
  }
  _setUpPageViewOnRouteChange () {
    this.router.afterEach(({ name }) => {
      if (this._config.excludedRoutes.length && this._config.excludedRoutes.indexOf(name) !== -1) {
        return
      }
      this.event('PageView')
    })
  }

  /**
   * Init facebook tracking pixel
   * @param  {String} appId
   * @param  {object} [data={}]
   */
  init (appId, data = {}) {
    injectFacebookPixelToWindow()
    if (!this._fbqEnabled()) {
      throw new FacebookPixelError("Facebook Pixel's base code not available")
    }

    if (this._config.debug) {
      console.log(
        `[Vue Facebook Pixel] Initializing app: ID: ${appId} Data: ${JSON.stringify(
          data
        )}`
      )
    }

    this.query('init', appId, data)
    // Fire 'PageView' event on first page/view
    if (this.automaticFiringOfPageViewEventsEnabled) {
      this.event('PageView')
    }
  }

  /**
   * Event tracking
   * @param  {String} name
   * @param  {object} [data={}]
   */
  event (name, data = {}) {
    if (!this._fbqEnabled()) return

    if (this._config.debug) {
      console.groupCollapsed(`[Vue Facebook Pixel] Track event "${name}"`)
      console.log(`With data: ${data}`)
      console.groupEnd()
    }

    this.query('track', name, data)
  }

  /**
   * Submit a raw query to fbq, for when the wrapper limits user on what they need.
   * This makes it still possible to access the plain Analytics api.
   * @param args
   */
  query (...args) {
    if (!this._fbqEnabled()) return

    if (this._config.debug) {
      console.groupCollapsed(`[Vue Facebook Pixel] Raw query`)
      console.log(`With data: `, ...args)
      console.groupEnd()
    }

    window.fbq(...args)
  }

  /**
   * Vue installer
   * @param  {Vue instance} Vue
   * @param  {Object} [options={}]
   */
  install (Vue, options = {}) {
    //
    const { router, debug, excludedRoutes, automaticFiringOfPageViewEventsEnabled } = options
    if (Array.isArray(excludedRoutes)) {
      this._config.excludedRoutes.concat(excludedRoutes)
    }
    this._config.debug = !!debug
    this.router = router
    this.automaticFiringOfPageViewEventsEnabled = automaticFiringOfPageViewEventsEnabled
    if (this.automaticFiringOfPageViewEventsEnabled && !this.router) {
      throw new FacebookPixelError('Automatic Firing Of Page View Events requested but no router supplied!')
    }

    // These objects may contain different providers as well,
    // or might be empty:
    if (!Vue.analytics) {
      Vue.analytics = {}
    }

    if (!Vue.prototype.$analytics) {
      Vue.prototype.$analytics = {}
    }

    // Setting values for both Vue and component instances
    // Usage:
    // 1. `Vue.analytics.fbq.init()`
    // 2. `this.$analytics.fbq.init()`
    Vue.analytics.fbq = { init: this.init, event: this.event, query: this.query }
    Vue.prototype.$analytics.fbq = { init: this.init, event: this.event, query: this.query }

    if (this.router && this.automaticFiringOfPageViewEventsEnabled) {
      this._setUpPageViewOnRouteChange()
    }
  }
}
export default FacebookPixel
