class FacebookPixelError extends Error {
  constructor (message, error) {
    super(error)
    this.name = 'FacebookPixelError'
    this.message = message
  }
}
export default FacebookPixelError
